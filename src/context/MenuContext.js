import React, { createContext, useState, useContext } from "react";
import { Children } from "react";

const MenuContext = createContext();

const MenuProvider = ({children}) => {
    const  [state, setState] = useState("start")
    return (
        <MenuContext.Provider value = {{state, setState}}> 
        {children}
        </MenuContext.Provider>
    )
}

export const useMenu = () => {
    const context = useContext(MenuContext)
    const {state, setState} = context
    return {state, setState}
}


export default MenuProvider;