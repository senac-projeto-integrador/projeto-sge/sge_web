import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";

import Api from "../api/api";
import "./clientData.css";

const ClientAdd = () => {
  const { register, handleSubmit } = useForm();
  const [list, setList] = useState();
  const [vehicle, setVehicle] = useState();
  const [spot, setSpot] = useState();
  const [client, setClient] = useState();

  const clientSubmit = async (data, event) => {
    event.preventDefault();
    const { model, color, license, category, typeVehicle } = data;

    const insertData = {
      model: model,
      color: color,
      license: license,
      category: category,
      typeVehicle: typeVehicle,
    };

    console.log(insertData);

    await Api.post("/vehicle", insertData);

    event.target.reset();
  };

  const onSubmit = async (data) => {
    const { vehicleId, boxId, clientId } = data;

    console.log(vehicleId, boxId, clientId);
    const includeData = {
      vehicleId: vehicleId,
      parkingSpotId: boxId,
      clientId: clientId,
      status: true,
    };
    await Api.post("parkingSpotHasVehicle", includeData);
  };

  const getList = async () => {
    try {
      const vehicleList = await Api.get("vehicle");
      console.log("veiculo", vehicleList.data);
      setVehicle(vehicleList.data);
    } catch (err) {
      console.error(err);
    }
  };

  const getSpot = async () => {
    try {
      const vehicleList = await Api.get("parkingSpot");
      console.log("Vaga", vehicleList.data);
      setSpot(vehicleList.data);
    } catch (err) {
      console.error(err);
    }
  };

  const getClient = async () => {
    try {
      const clients = await Api.get("client");
      console.log(clients.data);
      setClient(clients.data);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getList();
    getClient();
    getSpot();
  }, []);

  return (
    <>
      <div className="container">
        <h3>Inclusão na Vaga</h3>
        <form onSubmit={handleSubmit(onSubmit, window.event)}>
          <div className="col-sm-12 text-center">
            <label className="font-weight-bold">Veículo</label>
            <select className="ml-2" {...register("vehicleId")}>
              {vehicle &&
                vehicle.map((el) => (
                  <option value={el.id}>{el.license}</option>
                ))}
            </select>
            <label className="font-weight-bold ml-3">Vaga</label>
            <select className="ml-2" {...register("boxId")}>
              {spot &&
                spot.map((el) => <option value={el.id}>{el.name}</option>)}
            </select>

            <label className="font-weight-bold ml-3">Cliente</label>
            <select className="ml-2" {...register("clientId")}>
              {client &&
                client.map((el) => <option value={el.id}>{el.name}</option>)}
            </select>
            <button
              className="button-form mt-4 mb-5 rounded ml-4"
              type="submit"
            >
              Adicionar
            </button>
          </div>
        </form>
      </div>
      <div className="mt-1 text-center">
        <h3>Incluir Veículo</h3>
        <form onSubmit={handleSubmit(clientSubmit, window.event)}>
          <div className="col-sm-12 text-center">
            <label className="mr-2 font-weight-bold ">Modelo:</label>
            <input
              placeholder="Modelo"
              className="col-sm-3 rounded border mr-4"
              {...register("model")}
            />
            <label className="mr-2 ml-5 font-weight-bold">Cor: </label>
            <input
              placeholder="Cor"
              className="col-sm-3 rounded border"
              {...register("color")}
            />
          </div>

          <div className="col-sm-12 text-center ml-3">
            <label className="mr-2 font-weight-bold">Placa: </label>
            <input
              placeholder="Placa"
              className="col-sm-3 rounded border"
              {...register("license")}
            />
            <label className="mr-2 ml-5 font-weight-bold">Categoria:</label>
            <select
              className="col-sm-3 rounded border"
              {...register("category")}
            >
              <option value="hora">Hora</option>
              <option value="diaria">Diária</option>
              <option value="pernoite">Pernoite</option>
            </select>
          </div>
          <div className="col-sm-6 mr-5 ">
            <label className="mr-2 font-weight-bold ">Tipo Veic:</label>
            <select
              className="col-sm-3 rounded border"
              {...register("typeVehicle")}
            >
              <option value="carro">Carro</option>
              <option value="moto">Moto</option>
              <option value="camionete">Camionete</option>
            </select>
          </div>
          <button className="button-form mt-4 mb-5 rounded" type="submit">
            Incluir
          </button>
        </form>
      </div>
    </>
  );
};

export default ClientAdd;
