import React from "react";
import { useForm } from "react-hook-form";
import Api from "../api/api";

const Cashier = () => {
  const { register, handleSubmit } = useForm();

  const onSubmit = async (data, event) => {
    event.preventDefault();
    const { init, end, balance } = data;

    const insertData = {
      init: init,
      end: end,
      balance: balance,
    };

    await Api.post("/cashier", insertData);

    event.target.reset();
  };

  return (
    <div className="container">
      <h3 className="mt-1">Caixa</h3>
      <form className="" onSubmit={handleSubmit(onSubmit, window.event)}>
        <div className="col-sm-12 mt-5 d-flex align-items-center">
          <label className="col-sm-2">Valor Inicial:</label>
          <input
            placeholder="Valor Inicial"
            className="col-sm-3"
            {...register("init")}
          />
        </div>
        <div className="col-sm-12 mt-1 d-flex align-items-center">
          <label className="col-sm-2">Valor Final:</label>
          <input
            placeholder="Valor Final"
            className="col-sm-3"
            {...register("end")}
          />
        </div>
        <div className="col-sm-12 mt-1 d-flex align-items-center">
          <label className="col-sm-2">Saldo:</label>
          <input
            placeholder="Saldo"
            className="col-sm-3"
            {...register("balance")}
          />
        </div>
        <button className="button-form rounded mt-4" type="submit">
          Abrir
        </button>{" "}
      </form>
    </div>
  );
};

export default Cashier;
